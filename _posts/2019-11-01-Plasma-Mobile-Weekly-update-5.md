---
title:      "Plasma Mobile: weekly update: part 5"
created_at: 2019-11-01 15:00:00 UTC+2
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the fifth weekly blogpost. This week's update features various application polishing, better integration with GTK/GNOME applications and various fixes.

## Shell and user interface

Nicolas Fella simplified the [code of dialer](https://invent.kde.org/kde/plasma-phone-components/merge_requests/34) and [lockscreeen](https://invent.kde.org/kde/plasma-phone-components/merge_requests/35/).

breeze-gtk gained [support for libhandy widget style](https://phabricator.kde.org/D24786) make them better resemble the style and look-and-feel of Plasma Mobile. In postmarketOS a patch was added to [hide the close button in headerbar of GTK/GNOME applications](https://gitlab.com/postmarketOS/pmaports/merge_requests/702).

![GTK apps without headerbars](/img/w5_gtk_noheaders.jpg){: .blog-post-image-small}

## Applications

Jonah Brüchert fixed Plasma Camera to [display the proper symbolic icon in the global drawer.](https://invent.kde.org/kde/plasma-camera/commit/883f0028e5dc398515c8cd8cf26ab3b9ad11bfa0).

## MauiKit

The inline notification dialog in applications now has a dedicated button to trigger a response action instead of having to click the popup to perform such actions, which was unclear.

The flickable ToolBar now has edge shadows to indicate that the content goes beyond the border and can be flicked.

## Index

Components like the FileBrowser, used by Index and also by the FileDialog component, have been improved: the search now works as expected by also looking into the filename's suffixes. On abstract locations like tags:// or applications:// the search now works by filtering the content. The preferences like "sorting order" are now correctly saved per application using these components.

![Tags view](/img/screenshots/w5_index-tagview.png){: .blog-post-image-small}

The FileBrowser has been cleaned up visually. The configurable options have been moved to its own configuration dialog.

![Configuration dialog for Index](/img/screenshots/w5_index-settings.png){: .blog-post-image-small}

The Places sidebar now auto-refreshes when new tags are created. 

You can open a path-bar path in a different tab.

Menu items have been better organized and now have icons

## VVave

VVave now makes use of URL structures instead of strings to better identify local files from remote ones, this is some initial work in order to support streaming from remote locations.

Many parts have been moved to loaders to improve the performance and launch time by only loading them when needed.

The artwork is now once again correctly fetched from online sources, and the delegates are updated once new artwork has been found one by one, instead of reloading the whole set.

The main playlist sidebar no longer overlaps other content, like menus and dialogs.

![VVave playlist](/img/screenshots/w5_vvave-playlist.png){: .blog-post-image-small}

The tracklist and album grid delegates are using MauiKit item delegates for better visual integration with the other apps.

The floating disk is back, and it indicates a track is playing. By clicking on it the main playlist is revealed.

There is a new dedicated "focus" view which hides unnecessary controls.

![VVave focus mode](/img/screenshots/w5_vvave-focus.png){: .blog-post-image-small}

For a better experience tracks are now added and start playing once you tap on them, instead of just appending them to the main playlist.

The selection mode now works like it does on Index, you can select multiple tracks and the selected state is preserved.

![Selection mode](/img/screenshots/w5_vvave-selection-mode.png){: .blog-post-image-small}

Finally, VVave can now stream music files from the NextCloud Music app. You just need to set up a valid account and the tracks will appear under the Cloud view. This is still a work in progress.

![Cloud integration](/img/screenshots/w5_vvave-cloud.png){: .blog-post-image-small}

## Upstream

Jonah Brüchert [started upstreaming the patches to port the ofono-phonesim to Qt5](https://lists.ofono.org/hyperkitty/list/ofono@ofono.org/message/NMV4XUH7V3VCIPGBGPG4MBMXV2IBHBZN/). The QR-code scanner application [qrca](https://invent.kde.org/kde/qrca) and OTP client application [Keysmith](https://invent.kde.org/kde/keysmith) are now moved to the KDE namespace instead of their personal namespaces.

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
