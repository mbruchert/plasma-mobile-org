---
title: Contributing
permalink: /contributing/
layout: default
---

This article describes how you can contribute to Plasma Mobile, take
part in its design and development. This page addresses a wider range of
skills, each paragraph explains how to get you going from a different
starting point. Where else do you have the opportunity to shape a
complete Mobile software environment?

Current open tasks for Plasma Mobile are listed on [phabricator board](https://phabricator.kde.org/tag/plasma%3A_mobile/), If you want to navigate through the tasks listed by categories, you can use [interactive guide](/findyourway/).

I am a designer
===============

For designers, the [Plasma Mobile
Forums](https://forum.kde.org/viewforum.php?f=293) provide a good start
to get in touch with others in the team. You can pick up ideas from
there, and post feedback or even your own designs to get input from
others.

I want to write an app
======================

There are multiple ways to bring your application to Plasma Mobile

-   Write a native app: You can bring a new app that is already using
    KDE Frameworks to Plasma Mobile. Make the UI touch-friendly, and
    talk to us how it can best be integrated.

-   Port an existing app: Plasma Mobile is an inclusive system, allowing
    to run different apps. If your application is written using is ready
    for the use on touchscreens and high-dpi displays, it's usually
    possible to install it using the package manager. A first step is to
    try it and fix possible issues.

-   Android apps: Support for Android apps is currently
    work-in-progress. Talk to us about its status

I want to work on the Plasma Mobile system
==========================================

If you want to the Plasma Mobile “core” system or functionality, a good
starting point is to subscribe to the [Plasma mailing
list](https://mail.kde.org/mailman/listinfo/plasma-devel) and let us
know what you're interested in for improvements. If you're up for
picking up any other task, let us know and we'll get you going.

I want to make Plasma Mobile available on my Linux distribution
===============================================================

We love your effort to bring Plasma Mobile to a wider audience, and
we're ready to help you with that. If you would like to offer Plasma
Mobile on a not-yet supported Linux distribution, the following
information is useful:

-   The source code is hosted in git repositories at git.kde.org, an
    overview of relevant repositories can be found
    [here](https://community.kde.org/Plasma/Mobile/Code)

-   KDE provides a reference image based on KDE Neon, you can use this to
    test against, look at integration-level solutions and borrow its
    tricks to get Plasma Mobile running

-   If you want to improve things in an existing system, get in touch
    with us via the Plasma mailing list, or talk directly to the people
    working on the distro and packaging.

Do let us know of your efforts on the [Plasma mailing
list](https://mail.kde.org/mailman/listinfo/plasma-devel), so we can
help you to get going and provide advice that may save yourself time and
headaches.

I have this great idea for a new feature...
===========================================

...but I can't do it all by myself. A great way to find like-minded
people that may be able to help you make your idea a reality is to post
it to the [Plasma Mobile
Forums](https://forum.kde.org/viewforum.php?f=293) to gather feedback on
it. Maybe someone else has a similar goal, or you find people who want
to help you.

I just want to help, throw a task at me!
========================================

Great! We always need help. In order to find something that you find fun
and rewarding to work on, a good first step is to find out which itch
you have with Plasma Mobile, and how it can be scratched. What's nagging
you? Now give us a shout-out, best via the [Plasma mailing
list](https://mail.kde.org/mailman/listinfo/plasma-devel). You can also
make yourself known in the [Plasma Mobile
Forums](https://forum.kde.org/viewforum.php?f=293). There's plenty to
do, tasks for every skill and level, and you'll find it's fun to work on
and learn from each other.
